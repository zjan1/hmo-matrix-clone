# HMO App Quick Look

## Architecture

![HMO App Architecture](/documentation-assets/architecture.png)

### Frontend

#### Vue.js

> Vue.js is an open-source model–view–viewmodel JavaScript framework for building user interfaces and single-page applications.

The hmo-frontend application is built and served by `vue-cli`.

***NOTE:*** *We’ll want to move out of vue-cli for serving the application in production*

#### Bootstrap

> Bootstrap is a free and open-source CSS framework directed at responsive, mobile-first front-end web development.


#### Nginx

> Nginx is a web server which can also be used as a reverse proxy, load balancer, mail proxy and HTTP cache.

We use nginx as the cluster’s reverse proxy. This also allows us to treat the whole cluster like a single instance (since all requests are routed by nginx), making cross-service requests easy.

***NOTE:*** *We need to fix up security related concerns for production e.g. CORS, headers, etc...*

### Backend

#### Flask

> Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries. It has no database abstraction layer, form validation, or any other components where pre-existing third-party libraries provide common functions.

---

## Terminology

### Kubernetes

#### Deployment

A deployment is a Kubernetes object that manages pods. The most common use for a deployment is to keep a certain number of replicas but it is also useful for shared configuration e.g. environment variables.

#### Services

Since pods are ephemeral objects, their IPs (and even DNS names if CoreDNS is configured) are not static which makes service discovery difficult. A service groups together objects (can be deployments, replicasets, pods, etc) so you can access them via a single endpoint, making service discovery much easier.


---

## Common Operations

### Containerizing an Application

As an example, let's containerize a Flask app (like `hmo-solver` as shown here).

```
hmo-solver
├── Dockerfile
├── README.md
├── __init__.py
├── hmo_solver
│   ├── __init__.py
│   ├── app
│   │   ├── __init__.py
│   │   ├── plans.py
│   │   ├── solver.py
│   │   ├── utils.py
│   │   └── views.py
│   └── main.py
├── requirements.txt
└── setup.py

```

### Create the Dockerfile

Create the Dockerfile inside the app root directory. In this case, Dockerfile is already shown on directory tree for demonstration purposes.

### Dockerfile Contents

```Dockerfile
from python:3.9.0b3-alpine3.12

RUN apk add --no-cache  --virtual .build-deps gcc musl-dev curl

COPY ./requirements.txt .
RUN ["pip", "install", "-r", "requirements.txt"]

COPY . /hmo-solver

RUN ["pip", "install", "-e", "./hmo-solver"]
ENTRYPOINT ["python", "/hmo-solver/hmo_solver/main.py"]
```

This is the Dockerfile used to build the `hmo-solver` image. Let's breakdown the file line-by-line.

#### Base Image

```Dockerfile
from python:3.9.0b3-alpine3.12
```

The first part in building a Docker image is choosing the base image. For this application, since we know that we are serving a Python application, we chose to use the `python` image. To minimize size, we use the `alpine` version of the image.

This image provides us with a working Python environment so we can focus on setting up our application.

There are images for most popular environments. You can also use a Linux distro as a base image if the dependencies you need don't provide a Docker image.

#### Copy necessary files

```Dockerfile
COPY ./requirements.txt .
COPY . /hmo-solver
```

We need to copy all source files (i.e. python files), static assets (e.g. images), and other files needed to setup the application (e.g. requirements.txt for pip requirements).

#### Setup the Application

```Dockerfile
RUN apk add --no-cache  --virtual .build-deps gcc musl-dev curl
RUN ["pip", "install", "-r", "requirements.txt"]
RUN ["pip", "install", "-e", "./hmo-solver"]
```

Now that we have everything we need, we run the commands for installing dependencies and building the app.

The `RUN` command creates a new layer with the results of the operation. You want to use the `RUN` command (as opposed to the similar sounding `CMD`) when building.


#### Serving the Application

```Dockerfile
ENTRYPOINT ["python", "/hmo-solver/hmo_solver/main.py"]
```

The `ENTRYPOINT` determines the command the container will run once it's instantiated. In this case, we use `python .../main.py` to serve the application so it makes sense to use it as the entry point.

### Dockerfile Tips

#### Command Ordering

By default, Docker only rebuilds the parts of the application that have changed so in general, we want commands that are less likely to change to be placed first (a change in copied file contents counts as a change for the `COPY` command).

In the sample Dockerfile above, we copy the source files and run the pip install command for it right before the entry point because the application source files see the most frequent changes.

### Building and Running the Container

#### Build Command

```bash
 docker build <DOCKER_DIR> --tag <TAG_NAME>:<VERSION> 
```

#### Push Command

```bash
docker push <REGISTRY_ADDR>:<REGISTRY_PORT>/<TAG_NAME>:<VERSION>
```

#### Run Command


```bash
 docker run -p <TARGET_PORT>:<CONTAINER_PORT> <TAG_NAME>:<VERSION>
```
