#!/usr/bin/env bash


for template in `ls templates/* | grep deployment`
    do
      sed -e "s%CI_PROJECT_NAMESPACE%$CI_PROJECT_NAMESPACE%g" \
          -e "s%CI_REGISTRY_IMAGE%$CI_REGISTRY_IMAGE%g" \
          -i $template
      kubectl apply -f $template
    done
