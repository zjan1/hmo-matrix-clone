#!/usr/bin/env bash

if [[ ${RUN_TESTS} == "true" ]]; then
    for template in `ls templates/* | grep deployment`
        do
        sed -e "s%CI_PROJECT_NAMESPACE%$CI_PROJECT_NAMESPACE%g" \
            -e "s%CI_REGISTRY_IMAGE%$CI_REGISTRY_IMAGE%g" \
            -i $template
        kubectl apply -f $template
        done

    sleep 90

    kubectl get pods -o wide

    RESULT=`curl -LI http://a58474ae2d16c11eab0de0aae28472e5-1735119512.us-east-2.elb.amazonaws.com -o /dev/null -w '%{http_code}\n' -s`
    if [ $RESULT != '200' ]; then
        EXITCODE=1
    fi
    RESULT=`curl http://a75339b56ce0b11eab0de0aae28472e5-2073937715.us-east-2.elb.amazonaws.com/status`
    if [ $RESULT != 'OK' ]; then
        EXITCODE=1
    fi

    kubectl delete deployments front-nginx
    kubectl delete deployments hmo-frontend
    kubectl delete deployments hmo-solver

    exit $EXITCODE
else
    echo "Skipping tests!"
    exit 0
fi