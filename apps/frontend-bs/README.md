# Working pages:
Version 1.2
Contents Updated
- HMO Policy
> FAQs
> HMO Townhall
> HMO Misc
- Contact Us
> Added prescription psych


# Version

- MDBVue 6.7.1
- Vue 2.6.11

# Quick start

``` 
bash

# install dependencies
npm install / yarn

# serve with hot reload at localhost:8080
npm run demo

```

# Supported browsers

MDBootstrap supports the **latest, stable releases** of all major browsers and platforms.

Alternative browsers which use the latest version of WebKit, Blink, or Gecko, whether directly or via the platform’s web view API, are not explicitly supported. However, MDBootstrap should (in most cases) display and function correctly in these browsers as well.
