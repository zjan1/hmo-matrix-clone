class HMOPlan(object):
    name: str = ""
    principal_cost: float = None
    dependent_cost_map: dict = {}

    principal_member: dict = {}
    dependents: list = []

    def __init__(self, *args, **kwargs):
        args_map = {0: "principal_member", 1: "dependents"}
        args_dict = {args_map[i]: args[i] for i in range(len(args))}

        self.principal_member = args_dict.get("principal_member", self.principal_member)
        self.dependents = args_dict.get("dependents", self.dependents)

        # if kwargs are provided they will override passed args!
        self.principal_member = kwargs.get("principal_member", self.principal_member)
        self.dependents = kwargs.get("dependents", self.dependents)

        if not isinstance(self.dependents, list):
            self.dependents = [self.dependents]

    def solve_total_cost(self, x=None):
        # where x is the number of dependents
        if x is None:
            x = len(self.dependents)
        return self.dependent_cost_map.get(
            x, x * self.dependent_cost_map["extra"] + self.principal_cost
        )


class SuitePlan(HMOPlan):
    name: str = "Suite"
    principal_cost: float = 66943.00
    dependent_cost_map: dict = {
        1: 120666.00,
        2: 136697.00,
        3: 151896.00,
        4: 168756.00,
        "extra": 26566.00,
    }


class LargePlan(HMOPlan):
    name: str = "Large Private"
    principal_cost: float = 45733.00
    dependent_cost_map: dict = {
        1: 82493.00,
        2: 99287.00,
        3: 115212.00,
        4: 132875.00,
        "extra": 25841.00,
    }


class Regular250Plan(HMOPlan):
    name: str = "Regular Private (250)"
    principal_cost: float = 30096.00
    dependent_cost_map: dict = {
        1: 54391.00,
        2: 75224.00,
        3: 94982.00,
        4: 116839.00,
        "extra": 23542.00,
    }


class Regular200Plan(HMOPlan):
    name: str = "Regular Private (200)"
    principal_cost: float = 25379.00
    dependent_cost_map: dict = {
        1: 45908.00,
        2: 67649.00,
        3: 88266.00,
        4: 111131.00,
        "extra": 22517.00,
    }


# Senior Plans
class SeniorSuitePlan(HMOPlan):
    name: str = "Senior Suite"
    principal_cost: float = 104432.00


class SeniorLargePlan(HMOPlan):
    name: str = "Senior Large"
    principal_cost: float = 71344.00


class SeniorRegular250Plan(HMOPlan):
    name: str = "Senior Regular (250)"
    principal_cost: float = 39125.00


class SeniorRegular200Plan(HMOPlan):
    name: str = "Senior Regular (200)"
    principal_cost: float = 32993.00
