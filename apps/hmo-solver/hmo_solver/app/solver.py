from datetime import datetime
from hmo_solver.app.plans import (
    SuitePlan,
    LargePlan,
    Regular200Plan,
    Regular250Plan,
    SeniorSuitePlan,
    SeniorLargePlan,
    SeniorRegular250Plan,
    SeniorRegular200Plan,
)
from hmo_solver.app import db, models
from itertools import chain
from functools import reduce

plan_map = {
    "Suite": SuitePlan,
    "Large Private": LargePlan,
    "Regular Private (250)": Regular250Plan,
    "Regular Private (200)": Regular200Plan,
    "Senior Suite": SeniorSuitePlan,
    "Senior Large": SeniorLargePlan,
    "Senior Regular (250)": SeniorRegular250Plan,
    "Senior Regular (200)": SeniorRegular200Plan,
}


def is_eligible_senior(person: dict, age_range=[60, 65]):
    age = int(
        (
            datetime.now() - datetime.strptime(person["birthday"].strip(), "%Y-%m-%d")
        ).days
        / 365.25
    )

    return age >= age_range[0] and age <= age_range[1]


def is_eligible_dependent(employee: dict, person: dict):
    age = int(
        (
            datetime.now() - datetime.strptime(person["birthday"].strip(), "%Y-%m-%d")
        ).days
        / 365.25
    )

    def is_age_elibible(age: int, age_range):
        return age >= age_range[0] and age <= age_range[1]

    if employee["married"]:
        if person["relationship"] == "Spouse":
            return is_age_elibible(age, [18, 60])
        elif person["relationship"] == "Child":
            # has additional checks
            return is_age_elibible(age, [0, 18])
        else:
            return False
    else:
        if person["relationship"] == "Parent":
            # has additional checks
            return is_age_elibible(age, [18, 60])
        elif person["relationship"] == "Sibling":
            # has additional checks
            return is_age_elibible(age, [0, 18])
        else:
            return False


def group_dependents(applicants: dict):
    """
    Gives initial grouping of which applicants should be
    principal accounts and which should be dependent on a principal
    """
    eligible_dependents = [
        x
        for x in applicants["dependents"]
        if is_eligible_dependent(applicants["employee"], x)
    ]
    principal_dependents = [
        x
        for x in applicants["dependents"]
        if not is_eligible_dependent(applicants["employee"], x)
    ]

    return {
        "eligible_dependents": eligible_dependents,
        "principal_dependents": principal_dependents,
    }


def solve_single(applications, debug=False):
    # solved cases are mapped as x,y,z
    # where x is the number of principal dependents (18-60yrs old)
    # y is the number of seniors (60-65yrs)
    # z is the number of dependents (0-18)

    plans = []

    employee_plan = SuitePlan(principal_member=applications["employee"])

    if len(applications["dependents"]) == 0:
        return [employee_plan]

    dependent_groupings = group_dependents(applications)
    eligible_dependents = dependent_groupings["eligible_dependents"]

    # TODO: account for dependents > 65yrs old!
    principal_dependents = [
        x
        for x in dependent_groupings["principal_dependents"]
        if not is_eligible_senior(x)
    ]
    senior_dependents = [
        x for x in dependent_groupings["principal_dependents"] if is_eligible_senior(x)
    ]

    # principal, senior, dependent
    solutions = {
        "1,0,0": lambda x, y, z: [LargePlan(x[0])],
        "0,1,0": lambda x, y, z: [SeniorRegular250Plan(y[0])],
        "2,0,0": lambda x, y, z: [Regular200Plan(x[0]), Regular200Plan(x[1]),],
        "1,1,0": lambda x, y, z: [Regular200Plan(x[0]), SeniorRegular200Plan(y[1])],
        "0,2,0": lambda x, y, z: [
            SeniorRegular200Plan(y[0]),
            SeniorRegular200Plan(y[1]),
        ],
    }

    def suggest_for_unsolved(employee_plan, principals, seniors, dependents):
        plans = []

        principal_plans = [Regular200Plan(x) for x in principals]
        plans += principal_plans

        if len(dependents) > 0:
            # For unmarried employees, only siblings and parents (within the covered age range)
            # can be dependents. Given an application with at least one parent, and n>=1 other eligible dependents,
            # it'd be cheaper to register one parent as principal and register the other dependents under them
            # rather than applying for a Principal Plan for all of them
            # TODO: check if this holds true for a sibling being registered for a principal account
            #       trying to add another sibling as a dependent
            dependent_parents = [x for x in dependents if x["relationship"] == "Parent"]
            dependent_siblings = [
                x for x in dependents if x["relationship"] == "Sibling"
            ]

            if any(dependent_parents) and (
                len(dependent_parents) + len(dependent_siblings) >= 2
            ):
                parent_dependents = dependent_parents[1::] + dependent_siblings
                parent_plan = [Regular200Plan(dependent_parents[0], parent_dependents)]
                plans += parent_plan
            else:
                employee_plan.dependents = dependents
        else:
            employee_plan.dependents = dependents
        for senior in seniors:
            plans.append(SeniorRegular200Plan(senior))
        # done this way to bring employee_plan to front
        plans = [employee_plan] + plans
        return plans

    _code = [
        str(len(x))
        for x in [principal_dependents, senior_dependents, eligible_dependents]
    ]
    solution_code = ",".join(_code)

    if solution_code in solutions.keys():
        plans = [employee_plan]
        dependent_plans = solutions[solution_code](
            principal_dependents, senior_dependents, eligible_dependents,
        )
        for x in dependent_plans:
            plans.append(x)
    else:
        plans = suggest_for_unsolved(
            employee_plan, principal_dependents, senior_dependents, eligible_dependents
        )

    if debug:
        print("solution_code: {}".format(solution_code))
        print("plans: {}".format(plans))
    return plans


def solve_married(applications, debug=False):
    # employee plan depends on the number of dependents
    plan_map = {
        0: SuitePlan,
        1: SuitePlan,
        2: LargePlan,
        3: LargePlan,
        4: Regular250Plan,
        "extra": Regular250Plan,
    }

    dependent_groupings = group_dependents(applications)
    eligible_dependents = dependent_groupings["eligible_dependents"]

    employee_plan = plan_map.get(len(eligible_dependents), plan_map["extra"])(
        applications["employee"], eligible_dependents
    )

    if len(applications["dependents"]) == 0:
        return [employee_plan]

    # TODO: account for dependents > 65yrs old!
    principal_dependents = [
        x
        for x in dependent_groupings["principal_dependents"]
        if not is_eligible_senior(x)
    ]
    senior_dependents = [
        x for x in dependent_groupings["principal_dependents"] if is_eligible_senior(x)
    ]

    principal_plans = [Regular200Plan(x) for x in principal_dependents]
    senior_plans = [SeniorRegular200Plan(x) for x in senior_dependents]

    plans = [employee_plan] + principal_plans + senior_plans
    if debug:
        print("plans: {}".format(plans))
    return plans


def suggest_plan(applications, debug=False):
    # Employees with no plan dependents
    # are always assigned a SuitePlan

    employee = applications["employee"]
    plans = []

    dependent_groupings = group_dependents(applications)
    eligible_dependents = dependent_groupings["eligible_dependents"]

    if not employee["married"] or (
        employee["married"] and len(eligible_dependents) == 0
    ):
        plans = solve_single(applications)
    else:
        plans = solve_married(applications)

    plans = [
        {
            "planType": plan.name,
            "primary": plan.principal_member,
            "dependents": plan.dependents,
        }
        for plan in plans
    ]
    return plans


def compute_plan_price(applications, debug=False):
    """
    Returns the cost for the submitted application(s)
    input format
    [{
        planType: str,
        primary: [{
            name: str,
            bday: yyyy-mm-dd,
            employed: bool,
            married: bool,
        }],
        dependents:[{
            name: str,
            bday: yyyy-mm-dd,
            employed: bool,
            married: bool,
            relationship: str
        }],
        id: int,
    }]
    """
    applications = applications if isinstance(applications, list) else [applications]
    output = {}
    for application in applications:
        try:
            if len(application["primary"]) > 1:
                raise Exception("COMPUTE_ERROR: multiple primaries")
            plan = plan_map[application["planType"]](
                application["primary"][0], application["dependents"],
            )

            total_cost = plan.solve_total_cost()
            output[application["id"]] = {
                "cost": total_cost,
                "principal_cost": plan.principal_cost,
                "dependent_cost": total_cost - plan.principal_cost,
            }
        except Exception as e:
            error_message = "Invalid application details!"
            if "COMPUTE_ERROR" in str(e):
                error_message = str(e)
            output[application["id"]] = error_message
    _output = [value for key, value in output.items() if not isinstance(value, str)]
    output["total"] = sum([x["cost"] for x in _output])
    return output


def save_application(application, debug=False):
    """
    Saves the given application
    {
        planType: str,
        primary: [{
            name: str,
            bday: yyyy-mm-dd,
            employed: bool,
            married: bool,
            opswerksEmployee: bool,
        }],
        dependents:[{
            name: str,
            bday: yyyy-mm-dd,
            employed: bool,
            married: bool,
            relationship: str
        }],
        id: int,
        principalCost: float,
        dependentCost: float,
        totalCost: float,
    }
    """
    #  save user
    employee_data = application["primary"][0]

    employee = models.Applicant(
        name=employee_data["name"],
        birthday=datetime.strptime(employee_data["birthday"].strip(), "%Y-%m-%d"),
        relationship="None",
        employed=True,
        married=employee_data["married"],
        opswerksEmployee=True,
    )

    db.session.add(employee)

    #  save dependents
    dependent_data = application["dependents"]
    dependents = [
        models.Applicant(
            name=x["name"],
            birthday=datetime.strptime(x["birthday"].strip(), "%Y-%m-%d"),
            relationship="None",
            employed=True,
            married=x["married"],
            opswerksEmployee=False,
        )
        for x in dependent_data
    ]

    db.session.add_all(dependents)
    #  create application
    application = models.Application(plan_type=application["planType"],)

    db.session.add(application)
    db.session.flush()
    #  create associative entities

    employee_application = models.ApplicationApplicant(
        application_id=application.id,
        applicant_id=employee.id,
        applicant_type=models.APPLICANT_TYPES["primary"],
    )

    dependent_applications = [
        models.ApplicationApplicant(
            application_id=application.id,
            applicant_id=x.id,
            applicant_type=models.APPLICANT_TYPES["dependent"],
        )
        for x in dependents
    ]

    db.session.add(employee_application)
    db.session.add_all(dependent_applications)

    state = models.ApplicationState(
        application_id=application.id, state=models.APPLICATION_STATES["pending"]
    )

    db.session.add(state)

    # Single commit to avoid data errors if any one
    # of the models fail to instantiate
    db.session.commit()


def save_applications(applications, debug=False):
    if not isinstance(applications, list):
        applications = [applications]
    for application in applications:
        save_application(application, debug=debug)
    return "OK"


def get_applications(filters=[], debug=False):
    """
    Returns applications in this format
    applicantFormat = {
            name: str,
            bday: str,
            relationship: str,
            employed: bool,
            married: bool,
            opswerksEmployee: bool,
    }
    [{
        planType: str,
        primary: applicantFormat,
        dependents: [applicantFormat],
        state: str,
    }]
    """
    # TODO: implement filters
    applications = models.Application.query.all()

    return [application.json for application in applications]


def generate_csv(filters=[], debug=False):
    """
    Returns array (or arrays) that can easily be converted into csv
    """
    # TODO: implement filters
    applications = models.Application.query.all()

    return [models.Application.csv_headers()] + list(
        reduce(
            lambda x, y: x + y,
            list([application.get_csv(with_headers=False) for application in applications]),
            [],
        )
    )

