import os
import re
from setuptools import setup, find_packages

# Most of this is pretty standard for a python package

here = os.path.abspath(os.path.dirname(__file__))
readme_content = open(os.path.join(here, 'README.md')).read()

exclude_dirs = ['ez_setup', 'examples', 'tests', 'build', ]


def find_version(basedir):
    with open(os.path.join(basedir, 'version')) as f:
        return f.readline()

def parse_requirements(filename):
    """ load requirements from a pip requirements file """
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]


install_reqs = parse_requirements(os.path.join(here,'requirements.txt'))
reqs = install_reqs

setup(name='hmo_solver',
      description='HMO Matrix Application',
      version=find_version(here),
      long_description=readme_content,
      classifiers=[
          'Development Status :: 1 - Test',
          'Environment :: Console',
          'Intended Audience :: Programmers',
          'License :: Closed source',
          'Programming Language :: Python',
      ],
      keywords='HMO Matrix',
      author='opswerks',
      author_email='ph@opswerks.com',
      url='git@gitlab.com:opswerks/hmo-matrix-collection.git',
      license='Closed source',
      include_package_data=True,
      setup_requires=['pytest'],
      # tests_require=get_requirements(),
      install_requires=reqs,
      dependency_links=[],
      packages=find_packages(),
      entry_points={
          'console_scripts': [
              'app = hmo_solver.main',
          ],
      },
)
