Django==3.0.8
django-templated-mail==1.1.1
djangorestframework==3.11.0
djoser==2.0.3
django-cors-headers==3.4.0
django-finalware==1.0.0
django_createsuperuser
